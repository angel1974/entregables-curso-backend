package com.techuniversity.emp.utils;

public enum EstadosPedido {
    ACEPTADO,
    COCINADO,
    EN_ENTREGA,
    ENTREGADO,
    VALORADO;
}
