package com.techuniversity.emp.utils;

public class BadSeparator extends Exception {
    @Override
    public String getMessage(){
        return "El separador debe ser un carácter. No se admite espacio en blanco";
    }
}
